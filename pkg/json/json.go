package json

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
)

func ReadGenericArrayFromJson[T any](path string) *[]T {
	jsonFile, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	jsonData, err := io.ReadAll(jsonFile)
	if err != nil {
		log.Fatal(err)
	}
	jsonFile.Close()

	// check if file is empty
	if len(jsonData) == 0 {
		return &[]T{}
	}

	var data *[]T
	err = json.Unmarshal([]byte(jsonData), &data)
	if err != nil {
		log.Fatal(err)
	}

	return data
}

func WriteGenericDataToJson[T any](path string, mode rune, data ...T) {
	if mode != 'a' && mode != 'w' {
		fmt.Printf("The mode must be either 'w' for write or 'a' for append.")
		return
	}

	var structData []T
	if mode == 'a' {
		structData = *ReadGenericArrayFromJson[T](path)
		structData = append(structData, data...)
	} else {
		structData = append(structData, data...)
	}

	var jsonData []byte

	// When removing the last index of the file, we make
	// sure to write a valid json value, otherwise it
	// would print "null" to the file, and break the parser
	if len(structData) > 0 {
		jsonData, _ = json.MarshalIndent(structData, "", "  ")
	} else {
		jsonData = []byte("[]")
	}

	if err := os.WriteFile(path, jsonData, 0600); err != nil {
		fmt.Println("Error:", err)
	}
}
