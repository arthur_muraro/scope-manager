package env

import (
	"log"
	"os"
)

func GetHomeDir() string {
	HomeDir, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	return HomeDir
}

func CreateUtilsDirectory(path string) {
	makeDirectory(path)
}

func makeDirectory(path string) {
	if _, err := os.Stat(path); !os.IsNotExist(err) {
		return
	}

	// Create project directory
	err := os.Mkdir(path, 0700)
	if err != nil {
		log.Fatal(err)
	}
}

func MakeFile(path string) {
	// Check if scopes file exists
	if _, err := os.Stat(path); !os.IsNotExist(err) {
		return
	}

	// Create scopes file
	filePtr, err := os.Create(path)
	if err != nil {
		log.Fatal(err)
	}

	// Set POSIX rights
	err = os.Chmod(path, 0600)
	if err != nil {
		log.Fatal(err)
	}

	defer filePtr.Close()
}
