package scope

import (
	"fmt"

	"gitlab.com/arthur_muraro/scope-manager/pkg/json"
)

// Public Scope

type Scope struct {
	Id    int    `json:"id"`
	Value string `json:"value"`
}

func AddScope(path, Value string) {
	// Prevent Id duplication in database
	id := 1
	for {
		if ResolveScopeFromId(path, id) == "" {
			break
		}
		id++
	}

	json.WriteGenericDataToJson[Scope](path, 'a', Scope{Id: id, Value: Value})
}

func RemoveScope(path string, id int) {
	if ResolveScopeFromId(path, id) == "" {
		fmt.Printf("There is no value corresping to the id: %d\n", id)
		return
	}

	Scopes := *json.ReadGenericArrayFromJson[Scope](path)
	// Remove the Scope from the Scope list if it exists
	for i, item := range Scopes {
		if item.Id == id {
			Scopes[i] = Scopes[len(Scopes)-1]
			Scopes = Scopes[:len(Scopes)-1]
		}
	}

	json.WriteGenericDataToJson[Scope](path, 'w', Scopes...)
}

func ResolveScopeFromId(path string, id int) string {
	Scopes := json.ReadGenericArrayFromJson[Scope](path)
	for i := range *Scopes {
		if (*Scopes)[i].Id == id {
			return (*Scopes)[i].Value
		}
	}
	return ""
}
