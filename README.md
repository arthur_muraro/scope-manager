# scope-manager

A tool to manage the scope for you :)

It allows you to quickly store and retrieve *IPs*, *URLs* and *FQDNs* in a small personal *JSON* database.

All the scopes you add to scope-manager are stored in the `/home/user/.config/.scope-manager/` directory in the following files :
- `Ips.json`
- `Urls.json`
- `Fqdns.json`

The objective is to be as simple as possible, so scope-manager's output can be directly used in real commands.

## Install : 

```bash
go install gitlab.com/arthur_muraro/scope-manager/cmd/scope-manager@latest
```

## Usage :

```
scope-manager - a tool to remember the scope for you :)
        - aip        : Add an IP to the scope.
        - eip        : Echo the demanded IP, default value is 1 ( eg: scope-manager eip 3 ).
        - rip        : Remove the demanded IP, default value is 1 ( eg: scope-manager rip 3).
        - aurl       : Add a URL to the scope.
        - eurl       : Echo the demanded URL, default value is 1 ( eg: scope-manager eurl 3 ).
        - rurl       : Remove the demanded URL, default value is 1 ( eg: scope-manager rurl 3).
        - afqdn      : Add a FQDN to the scope.
        - efqdn      : Echo the demanded FQDN, default value is 1 ( eg: scope-manager efqdn 3 ).
        - rfqdn      : Remove the demanded FQDN, default value is 1 ( eg: scope-manager rfqdn 3).
        - list       : List the scope.
        - list-ips   : List the scope.
        - list-urls  : List the scope.
        - list-fqdns : List the scope.
        - flush      : Flush the scope.
        - help       : Print this menu.
```

## Example :

```bash
~$ aip 10.10.10.10
~$ eip
10.10.10.10
~$ aip 192.168.10.1
~$ eip 2
192.168.10.1
~$ aurl https://$(eip)/admin.php
~$ eurl
https://10.10.10.10/admin.php

~$ nmap $(eip)
```

### Aliases :

Personnaly I use some bash aliases to shorten the commands :

```bash
alias aip="scope-manager aip"
alias eip="scope-manager eip"
alias rip="scope-manager rip"

alias aurl="scope-manager aurl"
alias eurl="scope-manager eurl"
alias rurl="scope-manager rurl"

alias afqdn="scope-manager afqdn"
alias efqdn="scope-manager efqdn"
alias rfqdn="scope-manager rfqdn"

alias lips="scope-manager list-ips"
alias lurls="scope-manager list-urls"
alias lfqdns="scope-manager list-fqdns"

alias scope-list="scope-manager list"
alias scope-flush="scope-manager flush"
```

### TODO :

- [X] Fix remove command for ips and urls
- [X] Add lister for all scope categories
- [X] Beatufify lister
- [X] Handle error when trying to delete the last index of a file
- [X] Fix Id Duplication when removing an Id and than adding a new one