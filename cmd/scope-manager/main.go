package main

import (
	"fmt"
	"log"
	"os"
	filepath "path/filepath"
	"strconv"
	"strings"

	"gitlab.com/arthur_muraro/scope-manager/internal/env"
	"gitlab.com/arthur_muraro/scope-manager/internal/scope"
	"gitlab.com/arthur_muraro/scope-manager/pkg/json"
)

func RemoveFile(path string) {
	e := os.Remove(path)
	if e != nil {
		log.Fatal(e)
	}
}

func printHelp() {
	fmt.Println("scope-manager - a tool to remember the scope for you :)")
	fmt.Println("	- aip        : Add an IP to the scope.")
	fmt.Println("	- eip        : Echo the demanded IP, default value is 1 ( eg: scope-manager eip 3 ).")
	fmt.Println("	- rip        : Remove the demanded IP, default value is 1 ( eg: scope-manager rip 3).")
	fmt.Println("	- aurl       : Add a URL to the scope.")
	fmt.Println("	- eurl       : Echo the demanded URL, default value is 1 ( eg: scope-manager eurl 3 ).")
	fmt.Println("	- rurl       : Remove the demanded URL, default value is 1 ( eg: scope-manager rurl 3).")
	fmt.Println("	- afqdn      : Add a FQDN to the scope.")
	fmt.Println("	- efqdn      : Echo the demanded FQDN, default value is 1 ( eg: scope-manager efqdn 3 ).")
	fmt.Println("	- rfqdn      : Remove the demanded FQDN, default value is 1 ( eg: scope-manager rfqdn 3).")
	fmt.Println("	- list       : List the scope.")
	fmt.Println("	- list-ips   : List the scope.")
	fmt.Println("	- list-urls  : List the scope.")
	fmt.Println("	- list-fqdns : List the scope.")
	fmt.Println("	- flush      : Flush the scope.")
	fmt.Println("	- help       : Print this menu.")
}

func parse() {
	lenOfArgs := len(os.Args)
	if lenOfArgs > 3 || lenOfArgs < 2 {
		fmt.Println("scope-manager only takes 1 or 2 arguments...")
		printHelp()
		os.Exit(1)
	}

	var Id int
	if lenOfArgs == 2 {
		switch os.Args[1] {
		case "list":
			printScopeFile(IpsFile)
			printScopeFile(UrlsFile)
			printScopeFile(FqdnsFile)
			return
		case "list-ips":
			printScopeFile(IpsFile)
			return
		case "list-urls":
			printScopeFile(UrlsFile)
			return
		case "list-fqdns":
			printScopeFile(FqdnsFile)
			return
		case "flush":
			RemoveFile(IpsFile)
			RemoveFile(UrlsFile)
			RemoveFile(FqdnsFile)
			return
		case "help":
			printHelp()
			return
		}
		Id = 1
	}

	if lenOfArgs == 3 {
		switch os.Args[1] {
		case "aip":
			scope.AddScope(IpsFile, os.Args[2])
			return
		case "aurl":
			scope.AddScope(UrlsFile, os.Args[2])
			return
		case "afqdn":
			scope.AddScope(FqdnsFile, os.Args[2])
			return
		}
		var err error
		Id, err = strconv.Atoi(os.Args[2])
		if err != nil {
			fmt.Printf("Could not interpret %s as an integer value.", os.Args[2])
		}
	}

	// theses arguments take an optional value. That's why Id is set earlier depending on the number of arguments
	switch os.Args[1] {
	case "eip":
		fmt.Println(scope.ResolveScopeFromId(IpsFile, Id))
		return
	case "rip":
		scope.RemoveScope(IpsFile, Id)
		return
	case "eurl":
		fmt.Println(scope.ResolveScopeFromId(UrlsFile, Id))
		return
	case "rurl":
		scope.RemoveScope(UrlsFile, Id)
		return
	case "efqdn":
		fmt.Println(scope.ResolveScopeFromId(FqdnsFile, Id))
		return
	case "rfqdn":
		scope.RemoveScope(FqdnsFile, Id)
		return
	}
	printHelp()
}

func printScopeFile(path string) {
	scope := json.ReadGenericArrayFromJson[scope.Scope](path)
	if len(*scope) == 0 {
		return
	}
	fmt.Println("| -------------------------------------------------- |")
	fmt.Printf("|   %-48s |\n", strings.Split(filepath.Base(path), ".")[0])
	fmt.Println("| id | value                                         |")
	fmt.Println("| -- | --------------------------------------------- |")
	for i := range *scope {
		fmt.Printf("| %-2d | %-45s | \n", (*scope)[i].Id, (*scope)[i].Value)
	}
	fmt.Println("| -------------------------------------------------- |\n")
}

var Utilsdirectory string = env.GetHomeDir() + "/.config/.scope-manager/"
var IpsFile string = Utilsdirectory + "Ips.json"
var FqdnsFile string = Utilsdirectory + "Fqdns.json"
var UrlsFile string = Utilsdirectory + "Urls.json"

func SetEnv() {
	env.CreateUtilsDirectory(Utilsdirectory)
	env.MakeFile(IpsFile)
	env.MakeFile(FqdnsFile)
	env.MakeFile(UrlsFile)
}

func main() {
	SetEnv()

	parse()
}
